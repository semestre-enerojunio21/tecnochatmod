package com.example.tecnochat;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.os.Bundle;

import android.util.Log;
import android.view.View;

import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


public class ChatActivity extends AppCompatActivity {
    private ArrayList<String> listItems = new ArrayList<String>();
    private ArrayAdapter<String> adapter;

    private ListView chat;
    private TextView usuariosActivos;
    private HiloMensajes tareaHiloMensajes;

    private Handler mainHandler = new Handler();

    final Context context = this;

    SimpleDateFormat formatterMDY;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Intent intent = getIntent();
        // Se extraen los parametros de la Ventana Principal
        String direccionIP = intent.getStringExtra(MainActivity.EXTRA_DIP);
        String usuario = intent.getStringExtra(MainActivity.EXTRA_USR);
        String password = intent.getStringExtra(MainActivity.EXTRA_PAS);

        formatterMDY = new SimpleDateFormat("MM/dd/yyyy HH:mm");

        listItems = new ArrayList<String>();

        chat = findViewById(R.id.chat);
        usuariosActivos = findViewById(R.id.usuario_activos);

        // Se requiere un adaptador, para controlar el despliegue de los datos del arreglo en la Lista
        // el adaptador es como la clase Model que almacena los datos de un componente, en Java Standard
        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, listItems);

        // Se asocia el adaptador al ListView (chat)
        chat.setAdapter(adapter);

        String response = "";

        // Estas dos lineas se tienen que colocar para evitar un error de aviso de seguridad
        // Esta aplicacion se tiene que convertir a una implementacion con AsyncTask
        // Se deja así para que coincida con la implementación en la practica con Java Estandar
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Se crea la tarea que llevara la recepción de los mensajes
        tareaHiloMensajes = new HiloMensajes(direccionIP, 4444, usuario, password);
        tareaHiloMensajes.start(); // se inicia la tarea




        findViewById(R.id.btnEnviar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText edtMensaje = findViewById(R.id.edtMensaje);
                String mensaje = edtMensaje.getText().toString();

                addToList(mensaje); // agregamos el mensaje a la lista

                edtMensaje.setText(""); // borramos el texto de la caja de texto

                edtMensaje.onEditorAction(EditorInfo.IME_ACTION_DONE); // escondemos el teclado

                tareaHiloMensajes.enviar(mensaje); // enviamos el mensaje al servidor

            }
        });

    }
    void addToList(String respuesta) {
        String fechaHora = formatterMDY.format(Calendar.getInstance().getTime());
        listItems.add(fechaHora + " < " + respuesta);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onBackPressed() {

        tareaHiloMensajes.cerrar();

        super.onBackPressed();

    }
    public class HiloMensajes extends Thread {

        Socket clientSocket;
        PrintWriter out;
        BufferedReader in;
        String buffer;

        String direccionIP = "";
        int puerto = 0;
        String usuario = "";
        String password = "";

        ChatActivity parent = null;

        boolean salir = false;

        HiloMensajes(String _direccionIP, int _puerto, String _usuario, String _password) {

            direccionIP = _direccionIP;
            puerto = _puerto;
            usuario = _usuario;
            password = _password;
        }

        public void run() {

            try {
                clientSocket = new Socket(direccionIP, puerto);

                out = new PrintWriter(clientSocket.getOutputStream(), true);

                in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
                out.println(usuario);//Manda el nombre del usuario
                out.println(password); //Manda la contraseña
                buffer = in.readLine(); //Lee ok/contraseña incorrecta
                if(!buffer.equals("Autenticado!")){
                    alerta("Datos incorrectos", "Vuelva a intentarlo.");
                    cerrar();
                }
                while (!this.salir) {
                    buffer = in.readLine();
                    if (buffer != null) {
                        if(buffer.charAt(0) == '/'){
                            usuariosActivos.setText(buffer.substring(1));
                        }else{
                            this.addRespuesta(buffer);
                        }

                    }
                }
            } catch (final IOException ioe) {

                Log.v("IOExcepcion:", ioe.getMessage());

                alerta("IOExcepcion",ioe.getMessage());

            } catch (Exception e) {
                Log.v("Excepcion!:", e.getMessage());
                alerta("Excepcion",e.getMessage());
            }
        }

        void addRespuesta(final String mensaje){
            // Si no se manda a invocar el metodo addList
            // desde este Handler, marca un error ya que
            // el componente de la Lista no pertenece a este thread
            mainHandler.post(new Runnable(){
                @Override
                public void run() {
                    addToList(mensaje);
                }
            });
        }

        void enviar(String mensaje) {
            if (out!=null) {
                out.println(mensaje);
                if(mensaje.equals("salir")){
                    alerta("Hasta la proxima", "Volviendo al login.");
                }

            } else {
                alerta("Error","Sin conexión al servidor "+direccionIP+". Revise su estado de red y la dirección IP especificada.");
                Intent i = new Intent(ChatActivity.this, MainActivity.class);
                try {
                    Thread.sleep(2*1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                startActivity(i);
                ChatActivity.this.finish();
            }
        }

        void cerrar() {
            try {
                enviar("salir");
                this.salir = true;
                in.close();
                out.close();
                clientSocket.close();
            } catch (IOException ex) {
                Log.v("error al cerrar", ex.getMessage());
            }
        }
        void alerta(final String titulo, final String msg){

            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (ChatActivity.this!=null){

                        new AlertDialog.Builder(ChatActivity.this.context)
                                .setTitle(titulo)
                                .setMessage(msg)
                                .setCancelable(false)
                                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(ChatActivity.this!=null) {
                                            ChatActivity.this.finish();
                                        }
                                    }
                                }).show();
                    }
                }
            });

        }

    }
}